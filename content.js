function convertirExpressionEnDate(expression) {
	expression = expression.toLowerCase().trim();

	const maintenant = new Date();

	if (expression.includes("aujourd'hui")) {
		return maintenant;
	} else if (expression.includes("hier")) {
		maintenant.setDate(maintenant.getDate() - 1);
	} else if (expression.includes("demain")) {
		maintenant.setDate(maintenant.getDate() + 1);
	} else if (expression.includes("il y a")) {
		const morceaux = expression.split("il y a")[1].trim().split(/, | et /);
		for (const morceau of morceaux) {
			const morceauMatch = morceau.match(/(\d+) (mois|décennie(s)?|semaine(s)?|an(née)?(s)?|jour(s)?|heure(s)?|minute(s)?|seconde(s)?)/);
			if (morceauMatch) {
				const nombre = parseInt(morceauMatch[1]);
				const unite = morceauMatch[2];
				if (unite.match(/mois/)) {
					maintenant.setMonth(maintenant.getMonth() - nombre);
				} else if (unite.match(/décennie(s)?/)) {
					maintenant.setFullYear(maintenant.getFullYear() - nombre * 10);
				} else if (unite.match(/semaine(s)?/)) {
					maintenant.setDate(maintenant.getDate() - nombre * 7);
				} else if (unite.match(/an(née)?(s)?/)) {
					maintenant.setFullYear(maintenant.getFullYear() - nombre);
				} else if (unite.match(/jour(s)?/)) {
					maintenant.setDate(maintenant.getDate() - nombre);
				} else if (unite.match(/heure(s)?/)) {
					maintenant.setHours(maintenant.getHours() - nombre);
				} else if (unite.match(/minute(s)?/)) {
					maintenant.setMinutes(maintenant.getMinutes() - nombre);
				} else if (unite.match(/seconde(s)?/)) {
					maintenant.setSeconds(maintenant.getSeconds() - nombre);
				}
			}
		}
	} else {
		const parts = expression.split(" ");
		if (parts.length >= 2) {
			const jour = parseInt(parts[0]);
			const mois = parts[1];
			const annee = parts.length > 2 ? parseInt(parts[2]) : maintenant.getFullYear();
			const moisMapping = {
				"janvier": 0, "fevrier": 1, "mars": 2, "avril": 3, "mai": 4, "juin": 5,
				"juillet": 6, "aout": 7, "septembre": 8, "octobre": 9, "novembre": 10, "decembre": 11,
				"janvier": 0, "février": 1, "mars": 2, "avril": 3, "mai": 4, "juin": 5,
				"juillet": 6, "août": 7, "septembre": 8, "octobre": 9, "novembre": 10, "décembre": 11,
				"janv.": 0, "fév.": 1, "mar.": 2, "avr.": 3, "mai.": 4, "juin.": 5,
				"juil.": 6, "aoû.": 7, "sept.": 8, "oct.": 9, "nov.": 10, "déc.": 11,
				"jan.": 0, "fev.": 1, "mar.": 2, "avr.": 3, "mai.": 4, 
				"juil.": 6, "sep.": 8, "oct.": 9, "nov.": 10, "dec.": 11,
				"janv": 0, "fév": 1, "mar": 2, "avr": 3, "mai": 4, 
				"juil": 6, "aou": 7, "sept": 8, "oct": 9, "nov": 10, "déc": 11,
				"jan.": 0, "fev.": 1, "mar": 2, "avr": 3, "mai": 4,
				"juil": 6, "sep": 8, "oct": 9, "nov": 10, "dec": 11
			};
			
			const moisIndex = moisMapping[mois];
			if (moisIndex !== undefined) {
				const nouvelleDate = new Date(annee, moisIndex, jour);
				if (!isNaN(nouvelleDate)) {
					return nouvelleDate.toLocaleDateString('fr-FR', { day: 'numeric', month: 'long', year: 'numeric' });
				}
			}
		}
		return expression;
	}
	return maintenant.toLocaleDateString('fr-FR', { day: 'numeric', month: 'long', year: 'numeric' });
}

function timestampToSeconds(timestamp) {
	const components = timestamp.split(':').map(Number);

	if (components.length === 1) {
		const [seconds] = components;
		return seconds;
	} else if (components.length === 2) {
		const [minutes, seconds] = components;
		return minutes * 60 + seconds;
	} else if (components.length === 3) {
		const [hours, minutes, seconds] = components;
		return hours * 3600 + minutes * 60 + seconds;
	} else {
		return 0;
	}
}

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];
	var items = location.search.substr(1).split("&");
	for (var index = 0; index < items.length; index++) {
		tmp = items[index].split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	}
	return result;
}


function pushToClipBoard(button) {
	let info_channel = document.querySelector('ytd-watch-flexy .ytd-channel-name a').innerText.trim();
	let info_url = "https://www.youtube.com/watch?v=" + findGetParameter("v") ;
	let info_title = document.querySelector('#title>h1>yt-formatted-string').innerText.trim();
	let info_date = convertirExpressionEnDate(document.querySelector(".ytd-watch-info-text #tooltip").innerText.split("•")[1].replace("Diffusée en direct le ", "").replace("Diffusée en direct ", "").trim());
	let info_timestamp = timestampToSeconds(button.parentNode.querySelector('.segment-timestamp').innerText.trim());
	let info_transcript = button.parentNode.querySelector('.segment-text').innerText.trim();
	
	let clipboard_text = "\"''"+info_transcript+"''\"<ref>{{ReferenceText|desc="+info_channel+"|url="+info_url+"&t="+info_timestamp+"|url_desc="+info_title+"|date="+info_date+"|timestamp="+info_timestamp+"}}</ref>";
	
	navigator.clipboard.writeText(clipboard_text);
}

function handleTranscriptElements(addedTranscriptElements) {
	addedTranscriptElements.forEach(element => {
		if(!(element.querySelector("button"))){

			let button = document.createElement('button');
			button.textContent = "PolitiWiki";

			button.addEventListener("click", function(event) {
				let button = event.target;
				pushToClipBoard(button);
			});
			
			element.appendChild(button);

		}
	});
}
let observing = false;
function observeTranscriptElements() {
	const observer = new MutationObserver(function (mutationsList) {
		for (const mutation of mutationsList) {
			if (mutation.type === 'childList') {
				const addedTranscriptElements = Array.from(mutation.addedNodes).filter(
					(node) => node.classList && node.classList.contains('ytd-transcript-segment-renderer')
				);

				if (addedTranscriptElements.length > 0) {
					handleTranscriptElements(addedTranscriptElements);
				}
			}
		}
	});

	const body = document.body;
	if (body && !observing) {
		observing = true;
		observer.observe(body, { childList: true, subtree: true });
	}
}

observeTranscriptElements();
