# PolitiwiClip

PolitiwiClip permet de rapidement générer des refs pour PolitiWiki à partir d'une vidéo YouTube.

Extension Firefox disponible ici: https://addons.mozilla.org/en-US/firefox/addon/politiwiclip/

## Comment ça marche ?
Ouvrir le transcript de la video et survoler une phrase, un bouton PolitiWiki apparaît. Cliquer dessus pour copier dans le presse papier la phrase en question et la référence avec le timestamp et toutes les informations de la vidéo.
Editez la page PolitiWiki et coller pour ajouter la citation et sa référence.

Développé par un membre du chat de BadMulch, merci à lui pour le super travail !
